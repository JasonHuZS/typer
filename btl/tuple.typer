%%% tuple.typer --- Notation `_,_` for tuples

%% Here's an example similar to tuple from `load`
%%
%% Sample tuple: a module holding Bool and its constructors.
%%
%% We need the `?` metavars to be lexically outside of the
%% `typecons` expression, otherwise they end up generalized, so
%% we end up with a type constructor like
%%
%%     typecons _ (cons (τ₁ : Type) (t : τ₁)
%%                      (τ₂ : Type) (true : τ₂)
%%                      (τ₃ : Type) (false : τ₃))
%%
%% And it's actually even worse because it tries to generalize
%% over the level of those `Type`s, so we end up with an invalid
%% inductive type.
%%
%%     BoolMod = (##datacons
%%       ((lambda t1 t2 t3
%%         -> typecons _ (cons (t :: t1) (true :: t2) (false :: t3)))
%%            ? ? ?)
%%         cons)
%%       (_ := Bool) (_ := true) (_ := false);
%%

%% List_nth    = list.nth;
%% List_map    = list.map;
%% List_mapi   = list.mapi;
%% List_map2   = list.map2;
%% List_concat = list.concat;

%%
%% Move IO outside List (from element to List)
%%   (The function's type explain everything)
%%
io-list : List (IO ?a) -> IO (List ?a);
io-list l = let
  ff : IO (List ?a) -> IO ?a -> IO (List ?a);
  ff o v = do {
    o <- o;
    v <- v;
    IO_return (cons v o);
  };
in do {
  l <- (List_foldl ff (IO_return nil) l);
  IO_return (List_reverse l nil);
};

%%
%% Generate a List of pseudo-unique symbol
%%
%% Takes a List of Sexp and generate a List of new name of the same length
%% whatever are the element of the List
%%
gen-vars : List Sexp -> IO (List Sexp);
gen-vars vars = io-list (List_map
  (lambda _ -> gensym ())
  vars);

%%
%% Reference for tuple's implicit field name
%%
%% Takes a list of vars (it could actually only takes a length)
%% Returns symbol `%n` with n in [0,length) (integer only, obviously)
%%
gen-tuple-names : List Sexp -> List Sexp;
gen-tuple-names vars = List_mapi
  (lambda _ i -> Sexp_symbol (String_concat "%" (Int->String i)))
  vars;

%%
%% Takes a list
%% Returns a list of the same length with every element set to "?" symbol
%%
gen-deduce : List Sexp -> List Sexp;
gen-deduce vars = List_map
  (lambda _ -> Sexp_symbol "?")
  vars;

%%%
%%% Access one tuple's element
%%%

%%
%% This is a macro with conceptualy this signature:
%%     tuple-nth : (tup-type : Type) ≡> (elem-type : Type) ≡> tup-type -> Int -> elem-type;
%%
%% Returns the n'th element of the tuple
%%
tuple-nth = macro (lambda args -> let

  nerr = lambda _ -> (Int->Integer (-1));

  %% argument `n` of this macro
  n : Integer;
  n = Sexp_dispatch (List_nth 1 args Sexp_error)
    (lambda _ _ -> nerr ())
    nerr nerr
    (lambda n -> n)
    nerr nerr;

  %% implicit tuple field name
  elem-sym : Sexp;
  elem-sym = Sexp_symbol (String_concat "%" (Integer->String n));

  %% tuple, argument of this macro
  tup : Sexp;
  tup = List_nth 0 args Sexp_error;

in IO_return (Sexp_node (Sexp_symbol "__.__") (cons tup (cons elem-sym nil)))
);

%%%
%%% Affectation, unwraping tuple
%%%

%%
%% syntax:
%%     (x, y, z) <- p;
%%
%% and then `x`, `y`, `z` are defined as tuple's element 0, 1, 2
%%
assign-tuple = macro (lambda args -> let

  xserr = lambda _ -> (nil : List Sexp);

  %% Expect a ","-node
  %% Returns variables
  get-tup-elem : Sexp -> List Sexp;
  get-tup-elem sexp = Sexp_dispatch sexp
    (lambda s ss ->
      if (Sexp_eq s (Sexp_symbol "_,_")) then
        (ss)
      else
        (nil))
    xserr xserr xserr xserr xserr;

  %% map every tuple's variable
  %% using `tuple-nth` to assign element to variable
  mf : Sexp -> Sexp -> Int -> Sexp;
  mf t arg i = Sexp_node (Sexp_symbol "_=_")
    (cons arg (cons (Sexp_node (Sexp_symbol "tuple-nth")
      (cons t (cons (Sexp_integer (Int->Integer i)) nil))) nil));

in do {
  IO_return (Sexp_node (Sexp_symbol "_;_") (List_mapi
    (mf (List_nth 1 args Sexp_error))
    (get-tup-elem (List_nth 0 args Sexp_error))));
});

%%
%% Wrap the third argument `fun` with a `let` definition for each variables
%% Names are taken from `rvars` and definition are taken from `ivars`
%%

wrap-vars : List Sexp -> List Sexp -> Sexp -> Sexp;
wrap-vars ivars rvars fun = List_fold2 (lambda fun v0 v1 ->
  %%
  %% I prefer `let` definition because a lambda function would need a type
  %%     (quote ((lambda (uquote v0) -> (uquote fun)) (uquote v1))))
  %%
  (quote (let (uquote v1) = (uquote v0) in (uquote fun))))
  fun ivars rvars;

%%
%% Takes a list of values (expressions) as Sexp (like a variable, 1, 1.0, "str", etc)
%% Returns a tuple containing those values
%%
make-tuple-impl : List Sexp -> IO Sexp;
make-tuple-impl values = let

  %% map tuple element declaration
  mf1 : Sexp -> Sexp -> Sexp;
  mf1 name value = Sexp_node (Sexp_symbol "_::_") (cons name (cons value nil));

  %% map tuple element value
  mf2 : Sexp -> Sexp -> Sexp;
  mf2 value nth = Sexp_node (Sexp_symbol "_:=_") (cons nth (cons value nil));

  ff1 : Sexp -> Sexp -> Sexp -> Sexp;
  ff1 body arg arg-t = Sexp_node (Sexp_symbol "lambda_->_")
    (cons (Sexp_node (Sexp_symbol "_:_") (cons arg (cons arg-t nil)))
    (cons body nil));

  ff2 : Sexp -> Sexp -> Sexp;
  ff2 body arg = Sexp_node (Sexp_symbol "lambda_≡>_")
    (cons (Sexp_node (Sexp_symbol "_:_") (cons arg (cons (Sexp_symbol "Type") nil)))
    (cons body nil));

in do {
  args-t <- gen-vars values;

  args <- gen-vars values;

  names <- IO_return (gen-tuple-names values);

  tuple-t <- IO_return (Sexp_node (Sexp_symbol "typecons")
    (cons (Sexp_symbol "Tuple")
    (cons (Sexp_node (Sexp_symbol "cons") (List_map2 mf1 names args-t)) nil)));

  tuple <- IO_return (Sexp_node (Sexp_node (Sexp_symbol "datacons")
    (cons tuple-t (cons (Sexp_symbol "cons") nil)))
    (List_map2 mf2 args names));

  fun <- IO_return (List_foldl ff2
    (List_fold2 ff1 tuple (List_reverse args nil)
                          (List_reverse args-t nil))
    (List_reverse args-t nil));

  values <- IO_return (List_reverse values nil);

  affect <- IO_return (Sexp_node fun (List_reverse values nil));

  IO_return affect;
};

%%
%% Macro to instantiate a tuple
%%
make-tuple = macro (lambda args -> do {
  r <- make-tuple-impl args;
  r <- IO_return r;
  IO_return r;
});

%%
%% Macro returning the type of a tuple
%%
%% Takes element's type as argument
%%
tuple-type = macro (lambda args -> let

  mf : Sexp -> Sexp -> Sexp;
  mf n t = Sexp_node (Sexp_symbol "_::_")
    (cons n (cons t nil));

in do {
  names <- IO_return (gen-tuple-names args);

  r <- IO_return (Sexp_node (Sexp_symbol "typecons") (cons
    (Sexp_symbol "Tuple") (cons (Sexp_node (Sexp_symbol "cons")
    (List_map2 mf names args)) nil)));

  IO_return r;
});
