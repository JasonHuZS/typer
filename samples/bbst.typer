%%% bbst.typer --- Balanced Binary Search Tree

%% (this first implementation is a 2-3-4-tree)
%% (which is another representation for a Red Black Tree)

%% The 2-3-4 tree
%%
%% (Internal representation)

type BbsTree (a : Type)
  | node2 (d : a) (l0 : BbsTree a) (l1 : BbsTree a)
  | node3 (d0 : a) (d1 : a) (l0 : BbsTree a) (l1 : BbsTree a) (l2 : BbsTree a)
  | node4 (d0 : a) (d1 : a) (d2 : a)
    (l0 : BbsTree a) (l1 : BbsTree a) (l2 : BbsTree a) (l3 : BbsTree a)
  | node-leaf;

%%
%% The data structure with necessary data
%%
%% c is a compare function which should be similar to <= (or >=)
%%

type Bbst (a : Type)
  | bbst (s : Int) (c : a -> a -> Bool) (t : BbsTree a);

%%
%% Fold the tree in an unspecified order
%%

fold : (b : Type) ≡> (a : Type) ≡> (b -> a -> b) -> b -> Bbst a -> b;
fold f o t = let

  helper : (b : Type) ≡> (b -> a -> b) -> b -> BbsTree a -> b;
  helper f o t = case t
    | node2 d l0 l1 => let
        o1 = helper f o l0;
        o2 = helper f o1 l1;
      in f o2 d
    | node3 d0 d1 l0 l1 l2 => let
        o1 = helper f o l0;
        o2 = helper f o1 l1;
        o3 = helper f o2 l2;
        o4 = f o3 d0;
      in f o4 d1
    | node4 d0 d1 d2 l0 l1 l2 l3 => let
        o1 = helper f o l0;
        o2 = helper f o1 l1;
        o3 = helper f o2 l2;
        o4 = helper f o3 l3;
        o5 = f o4 d0;
        o6 = f o5 d1;
      in f o6 d2
    | node-leaf => o;

in case t
  | bbst _ c t => helper f o t;

%%
%% Fold the tree leftmost element first
%%
%% (If comparator is "<=" then it fold the smallest first)
%%

foldl : (b : Type) ≡> (a : Type) ≡> (b -> a -> b) -> b -> Bbst a -> b;
foldl f o t = let

  helper : (b : Type) ≡> (b -> a -> b) -> b -> BbsTree a -> b;
  helper f o t = case t
    | node2 d l0 l1 => let
        o1 = helper f o l0;
        o2 = f o1 d;
      in helper f o2 l1
    | node3 d0 d1 l0 l1 l2 => let
        o1 = helper f o l0;
        o2 = f o1 d0;
        o3 = helper f o2 l1;
        o4 = f o3 d1;
      in helper f o4 l2
    | node4 d0 d1 d2 l0 l1 l2 l3 => let
        o1 = helper f o l0;
        o2 = f o1 d0;
        o3 = helper f o2 l1;
        o4 = f o3 d1;
        o5 = helper f o4 l2;
        o6 = f o5 d2;
      in helper f o6 l3
    | node-leaf => o;

in case t
  | bbst _ c t => helper f o t;

%%
%% Fold the tree rightmost element first
%%
%% (If comparator is "<=" then it fold the biggest first)
%%

foldr : (b : Type) ≡> (a : Type) ≡> (b -> a -> b) -> b -> Bbst a -> b;
foldr f o t = let

  helper : (b : Type) ≡> (b -> a -> b) -> b -> BbsTree a -> b;
  helper f o t = case t
    | node2 d l0 l1 => let
        o1 = helper f o l1;
        o2 = f o1 d;
      in helper f o2 l0
    | node3 d0 d1 l0 l1 l2 => let
        o1 = helper f o l2;
        o2 = f o1 d1;
        o3 = helper f o2 l1;
        o4 = f o3 d0;
      in helper f o4 l0
    | node4 d0 d1 d2 l0 l1 l2 l3 => let
        o1 = helper f o l3;
        o2 = f o1 d2;
        o3 = helper f o2 l2;
        o4 = f o3 d1;
        o5 = helper f o4 l1;
        o6 = f o5 d0;
      in helper f o6 l0
    | node-leaf => o;

in case t
  | bbst _ c t => helper f o t;

%%
%% Get the number of element in the tree
%%

%% length = fold (lambda len _ -> len + 1) 0;

length : (a : Type) ≡> Bbst a -> Int;
length tree = case tree
  | bbst s _ _ => s;

%%
%% Is the tree empty?
%%

empty? tree = (Int_eq (length tree) 0);

%%
%% Check if two value are equal with ordering function only
%%
%% (As stated previously: ordering function must be like "<=" or ">=")
%%

comp-equal : (a : Type) ≡> (a -> a -> Bool) -> a -> a -> Bool;
comp-equal comp x y =
  if (comp x y) then
    (comp y x)
  else
    (false);

%%
%% Find an element in the tree and return it
%%

find : (a : Type) ≡> a -> Bbst a -> Option a;
find elem tree = let

  helper : (a -> a -> Bool) -> a -> BbsTree a -> Option a;
  helper comp elem tree = case tree
    | node2 d0 l0 l1 =>
      if (comp elem d0) then
        (if (comp d0 elem) then
          (some d0)
        else
          (helper comp elem l0))
      else
        (helper comp elem l1)
    | node3 d0 d1 l0 l1 l2 =>
      if (comp elem d0) then
        (if (comp d0 elem) then
          (some d0)
        else
          (helper comp elem l0))
      else
        (if (comp elem d1) then
          (if (comp d1 elem) then
            (some d1)
          else
            (helper comp elem l1))
        else
          (helper comp elem l2))
    | node4 d0 d1 d2 l0 l1 l2 l3 =>
      if (comp elem d0) then
        (if (comp d0 elem) then
          (some d0)
        else
          (helper comp elem l0))
      else
        (if (comp elem d1) then
          (if (comp d1 elem) then
            (some d1)
          else
            (helper comp elem l1))
         else
          (if (comp elem d2) then
            (if (comp d2 elem) then
              (some d2)
            else
              (helper comp elem l2))
          else
            (helper comp elem l3)))
    | node-leaf => none;

in case tree
  | bbst _ c t => helper c elem t;

%%
%% Is elem in the tree?
%%

member? : (a : Type) ≡> a -> Bbst a -> Bool;
member? elem tree = let

  oe = find elem tree;

in case oe
  | some _ => true
  | none => false;

%%
%% Insert an element in the tree
%%

insert : (a : Type) ≡> a -> Bbst a -> Bbst a;
insert elem tree = let

  helper : (a -> a -> Bool) -> a -> BbsTree a -> BbsTree a;
  helper comp e p = case p

    | node2 d0 k0 k1 =>
      if (comp e d0) then
        (if (comp d0 e) then
          (node2 e k0 k1)
         else
          (case k0
            | node4 e0 e1 e2 l0 l1 l2 l3 =>
              if (comp e e0) then
                (if (comp e0 e) then
                  (node2 d0 (node4 e e1 e2 l0 l1 l2 l3) k1)
                 else
                  (node3 e1 d0 (helper comp e (node2 e0 l0 l1)) (node2 e2 l2 l3) k1))
               else
                (if (comp e e1) then
                  (if (comp e1 e) then
                    (node2 d0 (node4 e0 e e2 l0 l1 l2 l3) k1)
                   else
                    (node3 e1 d0 (helper comp e (node2 e0 l0 l1)) (node2 e2 l2 l3) k1))
                 else
                  (if (comp e e2) then
                    (if (comp e2 e) then
                      (node2 d0 (node4 e0 e1 e l0 l1 l2 l3) k1)
                     else
                      (node3 e1 d0 (node2 e0 l0 l1) (helper comp e (node2 e2 l2 l3)) k1))
                   else
                    (node3 e1 d0 (node2 e0 l0 l1) (helper comp e (node2 e2 l2 l3)) k1)))
            | node-leaf => node3 e d0 node-leaf node-leaf k1
            | _ => node2 d0 (helper comp e k0) k1))
      else
        (case k1
          | node4 e0 e1 e2 l0 l1 l2 l3 =>
              if (comp e e0) then
                (if (comp e0 e) then
                  (node2 d0 k0 (node4 e e1 e2 l0 l1 l2 l3))
                 else
                  (node3 d0 e1 k0 (helper comp e (node2 e0 l0 l1)) (node2 e2 l2 l3)))
              else
                (if (comp e e1) then
                  (if (comp e1 e) then
                    (node2 d0 k0 (node4 e0 e e2 l0 l1 l2 l3))
                   else
                    (node3 d0 e1 k0 (helper comp e (node2 e0 l0 l1)) (node2 e2 l2 l3)))
                 else
                  (if (comp e e2) then
                    (if (comp e2 e) then
                      (node2 d0 k0 (node4 e0 e1 e l0 l1 l2 l3))
                     else
                      (node3 d0 e1 k0 (node2 e0 l0 l1) (helper comp e (node2 e2 l2 l3))))
                   else
                    (node3 d0 e1 k0 (node2 e0 l0 l1) (helper comp e (node2 e2 l2 l3)))))
          | node-leaf => node3 d0 e k0 node-leaf node-leaf
          | _ => node2 d0 k0 (helper comp e k1))

    %%
    %% When doing a recursion I try not to call helper on a node4
    %% Because we then would need the parent of this node4
    %%

    | node3 d0 d1 k0 k1 k2 =>
      if (comp e d0) then
        (if (comp d0 e) then
          (node3 e d1 k0 k1 k2)
         else
          (case k0
            | node4 e0 e1 e2 l0 l1 l2 l3 =>
              if (comp e e0) then
                (if (comp e0 e) then
                  (node3 d0 d1 (node4 e e1 e2 l0 l1 l2 l3) k1 k2)
                 else
                  (node4 e1 d0 d1 (helper comp e (node2 e0 l0 l1)) (node2 e2 l2 l3) k1 k2))
              else
                (if (comp e e1) then
                  (if (comp e1 e) then
                    (node3 d0 d1 (node4 e0 e e2 l0 l1 l2 l3) k1 k2)
                   else
                    (node4 e1 d0 d1 (helper comp e (node2 e0 l0 l1)) (node2 e2 l2 l3) k1 k2))
                 else
                  (if (comp e e2) then
                    (if (comp e2 e) then
                      (node3 d0 d1 (node4 e0 e1 e l0 l1 l2 l3) k1 k2)
                     else
                      (node4 e1 d0 d1 (node2 e0 l0 l1) (helper comp e (node2 e2 l2 l3)) k1 k2))
                   else
                    (node4 e1 d0 d1 (node2 e0 l0 l1) (helper comp e (node2 e2 l2 l3)) k1 k2)))
            | node-leaf => node4 e d0 d1 node-leaf node-leaf k1 k2
            | _ => node3 d0 d1 (helper comp e k0) k1 k2))
      else
        (if (comp e d1) then
          (if (comp d1 e) then
            (node3 d0 e k0 k1 k2)
           else
            (case k1
              | node4 e0 e1 e2 l0 l1 l2 l3 =>
                if (comp e e0) then
                  (if (comp e0 e) then
                    (node3 d0 d1 k0 (node4 e e1 e2 l0 l1 l2 l3) k2)
                   else
                    (node4 d0 e1 d1 k0 (helper comp e (node2 e0 l0 l1)) (node2 e2 l2 l3) k2))
                else
                  (if (comp e e1) then
                    (if (comp e1 e) then
                      (node3 d0 d1 k0 (node4 e0 e e2 l0 l1 l2 l3) k2)
                     else
                      (node4 d0 e1 d1 k0 (helper comp e (node2 e0 l0 l1)) (node2 e2 l2 l3) k2))
                   else
                    (if (comp e e2) then
                      (if (comp e2 e) then
                        (node3 d0 d1 k0 (node4 e0 e1 e l0 l1 l2 l3) k2)
                       else
                        (node4 d0 e1 d1 k0 (node2 e0 l0 l1) (helper comp e (node2 e2 l2 l3)) k2))
                     else
                      (node4 d0 e1 d1 k0 (node2 e0 l0 l1) (helper comp e (node2 e2 l2 l3)) k2)))
              | node-leaf => node4 d0 e d1 k0 node-leaf node-leaf k2
              | _ => node3 d0 d1 k0 (helper comp e k1) k2))
         else
          (case k2
            | node4 e0 e1 e2 l0 l1 l2 l3 =>
                if (comp e e0) then
                  (if (comp e0 e) then
                    (node3 d0 d1 k0 k1 (node4 e e1 e2 l0 l1 l2 l3))
                   else
                    (node4 d0 d1 e1 k0 k1 (helper comp e (node2 e0 l0 l1)) (node2 e2 l2 l3)))
                else
                  (if (comp e e1) then
                    (if (comp e1 e) then
                      (node3 d0 d1 k0 k1 (node4 e0 e e2 l0 l1 l2 l3))
                     else
                      (node4 d0 d1 e1 k0 k1 (helper comp e (node2 e0 l0 l1)) (node2 e2 l2 l3)))
                   else
                    (if (comp e e2) then
                      (if (comp e2 e) then
                        (node3 d0 d1 k0 k1 (node4 e0 e1 e l0 l1 l2 l3))
                       else
                        (node4 d0 d1 e1 k0 k1 (node2 e0 l0 l1) (helper comp e (node2 e2 l2 l3))))
                     else
                      (node4 d0 d1 e1 k0 k1 (node2 e0 l0 l1) (helper comp e (node2 e2 l2 l3)))))
            | node-leaf => node4 d0 d1 e k0 k1 node-leaf node-leaf
            | _ => node3 d0 d1 k0 k1 (helper comp e k2)))

    | node4 d0 d1 d2 k0 k1 k2 k3 =>
      %%
      %% I need the parent of `p`
      %% But it seems to work with other branch (see above)
      %%
      helper comp e (node2 d1 (node2 d0 k0 k1) (node2 d2 k2 k3))

    | node-leaf => node2 e node-leaf node-leaf;

in if (member? elem tree) then
     (case tree % we may want to replace an element (partial comparison function)
       | bbst s c t => bbst s c (helper c elem t))
   else
     (case tree
       | bbst s c t => bbst (s + 1) c (helper c elem t));

%%
%% Internal function used in `remove` and `pop-leftmost`
%%

pop-leftmost' : (a : Type) ≡> (a -> a -> Bool) -> BbsTree a -> BbsTree a;
pop-leftmost' comp tree = let

  four2three : a -> a -> a ->
    BbsTree a -> BbsTree a -> BbsTree a -> BbsTree a -> BbsTree a;

  three2two : a -> a ->
    BbsTree a -> BbsTree a -> BbsTree a -> BbsTree a;

  two2leaf : a -> BbsTree a -> BbsTree a -> BbsTree a;

  helper : BbsTree a -> BbsTree a;

  helper tree = case tree
    | node2 d l0 l1 =>
      two2leaf d l0 l1
    | node3 d0 d1 l0 l1 l2 =>
      three2two d0 d1 l0 l1 l2
    | node4 d0 d1 d2 l0 l1 l2 l3 =>
      four2three d0 d1 d2 l0 l1 l2 l3
    | node-leaf => node-leaf;

  two2leaf d0 l0 l1 = case l0
    | node-leaf => l1
    | _ => node2 d0 (helper l0) l1;

  three2two d0 d1 l0 l1 l2 = case l0
    | node-leaf => node2 d1 l1 l2
    | _ => node3 d0 d1 (helper l0) l1 l2;

  four2three d0 d1 d2 l0 l1 l2 l3 = case l0
    | node-leaf => node3 d1 d2 l1 l2 l3
    | _ => node4 d0 d1 d2 (helper l0) l1 l2 l3;

in (helper tree);

%%
%% Remove the smallest element (when using "<=")
%%

pop-leftmost : (a : Type) ≡> Bbst a -> Bbst a;
pop-leftmost t = case t
  | bbst s c t => if (Int_eq 0 s) then
                    (bbst s c t)
                  else
                    (bbst (s - 1) c (pop-leftmost' c t));

%%
%% Internal used in `remove` and `pop-rightmost`
%%

pop-rightmost' : (a : Type) ≡> (a -> a -> Bool) -> BbsTree a -> BbsTree a;
pop-rightmost' comp tree = let

  four2three : a -> a -> a ->
    BbsTree a -> BbsTree a -> BbsTree a -> BbsTree a -> BbsTree a;

  three2two : a -> a ->
    BbsTree a -> BbsTree a -> BbsTree a -> BbsTree a;

  two2leaf : a -> BbsTree a -> BbsTree a -> BbsTree a;

  helper : BbsTree a -> BbsTree a;

  helper tree = case tree
    | node2 d l0 l1 =>
      two2leaf d l0 l1
    | node3 d0 d1 l0 l1 l2 =>
      three2two d0 d1 l0 l1 l2
    | node4 d0 d1 d2 l0 l1 l2 l3 =>
      four2three d0 d1 d2 l0 l1 l2 l3
    | node-leaf => node-leaf;

  two2leaf d0 l0 l1 = case l1
    | node-leaf => l0
    | _ => node2 d0 l0 (helper l1);

  three2two d0 d1 l0 l1 l2 = case l2
    | node-leaf => node2 d0 l0 l1
    | _ => node3 d0 d1 l0 l1 (helper l2);

  four2three d0 d1 d2 l0 l1 l2 l3 = case l3
    | node-leaf => node3 d0 d1 l0 l1 l2
    | _ => node4 d0 d1 d2 l0 l1 l2 (helper l3);

in (helper tree);

%%
%% Remove the greatest element (when using "<=")
%%

pop-rightmost : (a : Type) ≡> Bbst a -> Bbst a;
pop-rightmost t = case t
  | bbst s c t => if (Int_eq 0 s) then
                    (bbst s c t)
                  else
                    (bbst (s - 1) c (pop-rightmost' c t));

%%
%% Internal used in `remove` and `get-leftmost`
%%

get-leftmost' : (a : Type) ≡> (a -> a -> Bool) -> BbsTree a -> Option a;
get-leftmost' comp tree = let

  four2three : a -> a -> a ->
    BbsTree a -> BbsTree a -> BbsTree a -> BbsTree a -> Option a;

  three2two : a -> a -> BbsTree a -> BbsTree a -> BbsTree a -> Option a;

  two2leaf : a -> BbsTree a -> BbsTree a -> Option a;

  helper : BbsTree a -> Option a;

  helper tree = case tree
    | node2 d l0 l1 =>
      two2leaf d l0 l1
    | node3 d0 d1 l0 l1 l2 =>
      three2two d0 d1 l0 l1 l2
    | node4 d0 d1 d2 l0 l1 l2 l3 =>
      four2three d0 d1 d2 l0 l1 l2 l3
    | node-leaf => none;

  two2leaf d0 l0 l1 = case l0
    | node-leaf => some d0
    | _ => (helper l0);

  three2two d0 d1 l0 l1 l2 = case l0
    | node-leaf => some d0
    | _ => (helper l0);

  four2three d0 d1 d2 l0 l1 l2 l3 = case l0
    | node-leaf => some d0
    | _ => (helper l0);

in (helper tree);

%%
%% Get smallest element (when using "<=")
%%

get-leftmost : (a : Type) ≡> Bbst a -> Option a;
get-leftmost t = case t
  | bbst _ c t => get-leftmost' c t;

%%
%% Internal used in `remove` and `get-rightmost`
%%

get-rightmost' : (a : Type) ≡> (a -> a -> Bool) -> BbsTree a -> Option a;
get-rightmost' comp tree = let

  four2three : a -> a -> a ->
    BbsTree a -> BbsTree a -> BbsTree a -> BbsTree a -> Option a;

  three2two : a -> a -> BbsTree a -> BbsTree a -> BbsTree a -> Option a;

  two2leaf : a -> BbsTree a -> BbsTree a -> Option a;

  helper : BbsTree a -> Option a;

  helper tree = case tree
    | node2 d l0 l1 =>
      two2leaf d l0 l1
    | node3 d0 d1 l0 l1 l2 =>
      three2two d0 d1 l0 l1 l2
    | node4 d0 d1 d2 l0 l1 l2 l3 =>
      four2three d0 d1 d2 l0 l1 l2 l3
    | node-leaf => none;

  two2leaf d0 l0 l1 = case l1
    | node-leaf => some d0
    | _ => (helper l1);

  three2two d0 d1 l0 l1 l2 = case l2
    | node-leaf => some d1
    | _ => (helper l2);

  four2three d0 d1 d2 l0 l1 l2 l3 = case l3
    | node-leaf => some d2
    | _ => (helper l3);

in (helper tree);

%%
%% Get greatest element (when using "<=")
%%

get-rightmost : (a : Type) ≡> Bbst a -> Option a;
get-rightmost t = case t
  | bbst _ c t => get-rightmost' c t;

%%
%% Remove an element from the tree
%%
%% This implementation isn't always balanced
%% I may remove a node2 when it has a leaf child but I should
%% do some tricks with sibling to always be balanced
%%

remove : (a : Type) ≡> a -> Bbst a -> Bbst a;
remove elem tree = let

  rebuild-two : (a -> a -> Bool) ->
    BbsTree a -> BbsTree a -> BbsTree a;

  rebuild-two comp l0 l1 = case l0
    | node-leaf => ( case l1
      | node-leaf => node-leaf
      | _ => let
          oe = get-leftmost' comp l1;
        in case oe
          | some e => node2 e l0 (pop-leftmost' comp l1)
          | none => node-leaf ) % I think this case may not happen
    | _ => let
        oe = get-rightmost' comp l0;
      in case oe
        | some e => node2 e (pop-rightmost' comp l0) l1
        | none => node-leaf; % May not happen too

  rebuild-left-three : (a -> a -> Bool) -> a ->
    BbsTree a -> BbsTree a -> BbsTree a -> BbsTree a;

  rebuild-left-three comp d1 l0 l1 l2 = case l0
    | node-leaf => ( case l1
      | node-leaf => node2 d1 node-leaf l2
      | _ => let
          oe = get-leftmost' comp l1;
        in case oe
          | some e => node3 e d1 l0 (pop-leftmost' comp l1) l2
          | none => node-leaf ) % I think this case may not happen
    | _ => let
        oe = get-rightmost' comp l0;
      in case oe
        | some e => node3 e d1 (pop-rightmost' comp l0) l1 l2
        | none => node-leaf; % May not happen too

  rebuild-right-three : (a -> a -> Bool) -> a ->
    BbsTree a -> BbsTree a -> BbsTree a -> BbsTree a;

  rebuild-right-three comp d0 l0 l1 l2 = case l1
    | node-leaf => ( case l2
      | node-leaf => node2 d0 l0 node-leaf
      | _ => let
          oe = get-leftmost' comp l2;
        in case oe
          | some e => node3 d0 e l0 l1 (pop-leftmost' comp l2)
          | none => node-leaf ) % I think this case may not happen
    | _ => let
        oe = get-rightmost' comp l1;
      in case oe
        | some e => node3 d0 e l0 (pop-rightmost' comp l1) l2
        | none => node-leaf; % May not happen too

  rebuild-left-four : (a -> a -> Bool) -> a -> a ->
    BbsTree a -> BbsTree a -> BbsTree a -> BbsTree a -> BbsTree a;

  rebuild-left-four comp d1 d2 l0 l1 l2 l3 = case l0
    | node-leaf => ( case l1
      | node-leaf => node3 d1 d2 node-leaf l2 l3
      | _ => let
          oe = get-leftmost' comp l1;
        in case oe
          | some e => node4 e d1 d2 l0 (pop-leftmost' comp l1) l2 l3
          | none => node-leaf ) % I think this case may not happen
    | _ => let
        oe = get-rightmost' comp l0;
      in case oe
        | some e => node4 e d1 d2 (pop-rightmost' comp l0) l1 l2 l3
        | none => node-leaf; % May not happen too

  rebuild-middle-four : (a -> a -> Bool) -> a -> a ->
    BbsTree a -> BbsTree a -> BbsTree a -> BbsTree a -> BbsTree a;

  rebuild-middle-four comp d0 d2 l0 l1 l2 l3 = case l1
    | node-leaf => ( case l2
      | node-leaf => node3 d0 d2 l0 node-leaf l3
      | _ => let
          oe = get-leftmost' comp l2;
        in case oe
          | some e => node4 d0 e d2 l0 l1 (pop-leftmost' comp l2) l3
          | none => node-leaf ) % I think this case may not happen
    | _ => let
        oe = get-rightmost' comp l1;
      in case oe
        | some e => node4 d0 e d2 l0 (pop-rightmost' comp l1) l2 l3
        | none => node-leaf; % May not happen too

  rebuild-right-four : (a -> a -> Bool) -> a -> a ->
    BbsTree a -> BbsTree a -> BbsTree a -> BbsTree a -> BbsTree a;

  rebuild-right-four comp d0 d1 l0 l1 l2 l3 = case l2
    | node-leaf => ( case l3
      | node-leaf => node3 d0 d1 l0 l1 node-leaf
      | _ => let
          oe = get-leftmost' comp l3;
        in case oe
          | some e => node4 d0 d1 e l0 l1 l2 (pop-leftmost' comp l3)
          | none => node-leaf ) % I think this case may not happen
    | _ => let
        oe = get-rightmost' comp l2;
      in case oe
        | some e => node4 d0 d1 e l0 l1 (pop-rightmost' comp l2) l3
        | none => node-leaf; % May not happen too

  helper : (a -> a -> Bool) -> a -> BbsTree a -> BbsTree a;
  helper comp elem tree = case tree
    | node2 d l0 l1 =>

      if (comp elem d) then
        (if (comp d elem) then
          (rebuild-two comp l0 l1)
         else
          (node2 d (helper comp elem l0) l1))
      else
        (node2 d l0 (helper comp elem l1))

    | node3 d0 d1 l0 l1 l2 =>

      if (comp elem d0) then
        (if (comp d0 elem) then
          (rebuild-left-three comp d1 l0 l1 l2)      % replace d0
         else
          (node3 d0 d1 (helper comp elem l0) l1 l2))
      else
        (if (comp elem d1) then
          (if (comp d1 elem) then
            (rebuild-right-three comp d0 l0 l1 l2)   % replace d1
           else
            (node3 d0 d1 l0 (helper comp elem l1) l2))
         else
          (node3 d0 d1 l0 l1 (helper comp elem l2)))

    | node4 d0 d1 d2 l0 l1 l2 l3 =>

      if (comp elem d0) then
        (if (comp d0 elem) then
          (rebuild-left-four comp d1 d2 l0 l1 l2 l3)
         else
          (node4 d0 d1 d2 (helper comp elem l0) l1 l2 l3))
      else
        (if (comp elem d1) then
          (if (comp d1 elem) then
            (rebuild-middle-four comp d0 d2 l0 l1 l2 l3)
           else
            (node4 d0 d1 d2 l0 (helper comp elem l1) l2 l3))
         else
          (if (comp elem d2) then
            (if (comp d2 elem) then
              (rebuild-right-four comp d0 d1 l0 l1 l2 l3)
             else
              (node4 d0 d1 d2 l0 l1 (helper comp elem l2) l3))
           else
            (node4 d0 d1 d2 l0 l1 l2 (helper comp elem l3))))

    | node-leaf => node-leaf;

in if (member? elem tree) then
     (case tree
       | bbst s c t => bbst (s - 1) c (helper c elem t))
   else
     (tree);

%%
%% An empty tree with a key compare function as argument
%%

empty : (a : Type) ≡> (a -> a -> Bool) -> Bbst a;
empty comp-fun = bbst 0 comp-fun node-leaf;

%%%
%%% Ordered dictionary
%%%

odict-comp : (Key : Type) ≡> (Data : Type) ≡> (Key -> Key -> Bool) ->
  ((Pair Key (Option Data)) -> (Pair Key (Option Data)) -> Bool);
odict-comp c = (lambda p0 -> (lambda p1 ->
  case p0 | pair k0 _ =>
    case p1 | pair k1 _ => c k0 k1));

odict : (Key : Type) ≡> (Data : Type) ≡> (Key -> Key -> Bool) ->
  Bbst (Pair Key (Option Data));
odict c = bbst 0 (odict-comp c) node-leaf;

odict-length : (Key : Type) ≡> (Data : Type) ≡> Bbst (Pair Key (Option Data)) -> Int;
odict-length t = length t;

odict-find : (Key : Type) ≡> (Data : Type) ≡> Key ->
  Bbst (Pair Key (Option Data)) -> Option Data;
odict-find k t = case (find (pair k none) t)
  | some (pair _ d) => d
  | none => none;

odict-insert : (Key : Type) ≡> (Data : Type) ≡> Key -> Data ->
  Bbst (Pair Key (Option Data)) -> Bbst (Pair Key (Option Data));
odict-insert k d t = insert (pair k (some d)) t;

odict-member? : (Key : Type) ≡> (Data : Type) ≡> Key ->
  Bbst (Pair Key (Option Data)) -> Bool;
odict-member? k t = member? (pair k none) t;

odict-remove : (Key : Type) ≡> (Data : Type) ≡> Key ->
  Bbst (Pair Key (Option Data)) -> Bbst (Pair Key (Option Data));
odict-remove k t = remove (pair k none) t;

odict-update : (Key : Type) ≡> (Data : Type) ≡> Key -> Data ->
  Bbst (Pair Key (Option Data)) -> Bbst (Pair Key (Option Data));
odict-update k d t = insert (pair k (some d)) t;

odict-empty? : (Key : Type) ≡> (Data : Type) ≡> Bbst (Pair Key (Option Data)) -> Bool;
odict-empty? t = empty? t;

%%%
%%% Test helper
%%%

%%
%% Compare function for Int
%%

int-comp : Int -> Int -> Bool;
int-comp x y = Int_<= x y;

%%
%% Empty Int tree
%%

int-empty : Bbst Int;
int-empty = empty int-comp;

%%
%% Construct a set with Int in interval [min,max[
%%
%% (This is the worst case for a binary tree)
%%

int-range1 : Int -> Int -> Bbst Int;
int-range1 min max = let

  helper : Int -> Bbst Int -> Bbst Int;
  helper min tree = if (Int_eq min max) then
    (tree) else (helper (min + 1) (insert min tree));

in helper min (empty int-comp);

%%
%% Construct a set with Int in interval [min,max[
%%
%% (This one is not the worst case)
%%

int-range2 : Int -> Int -> Bbst Int;
int-range2 min max = let

  helper : Int -> Int -> Bbst Int -> Bbst Int;
  helper min max tree = let

    a = insert (max - 1) tree;

  in if (Int_eq min max) then
       (a)
     else
       (helper (min + 1) (max - 1) (insert min a));

in helper min max (empty int-comp);

%%
%% Remove Int inside interval [min,max[ from the Tree
%%

remove-range : Int -> Int -> Bbst Int -> Bbst Int;
remove-range min max tree = let

  helper : Int -> Bbst Int -> Bbst Int;
  helper min tree = if (Int_eq min max) then
    (tree) else (helper (min + 1) (remove min tree));

in helper min tree;
